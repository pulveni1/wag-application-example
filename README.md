# WAG CRUD Example #

This example is based on WAG and includes almost everything you will need for next Novartis application.

### Included in this example ###

* nxc-core
* header with navigation and GetHelp
* NIBR footer
* CRUD implementation (Create, Read, Update Delete)
* handling fetching state example
* error handling for CRUD
* centralized notification system
* custom routing example (routes called programmatically)
* Redux DevTools
* people suggest

### How do I get set up? ###

* Install development environment
* Clone this repo: ```git clone https://gassean2@bitbucket.org/gassean2/wag-application-example.git```
* Open "wag-application-example" folder in terminal and run ```npm install``

### How do I run this example? ###
* Open project folder in terminal
* run ```npm start```
* [in Chrome] press ctrl+h for ReduxDevtools or use your browser dev tools

### Contribution guidelines ###

* Create branch for new feature
* Open PullRequest for branch
* Pass Code review
* Be happy

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Special thanks to ###
* Lorenz Blum
* Patrick Pfyffer
