import {
    GET_IDEAS,
    GET_IDEAS_SUCCESS,
    GET_IDEAS_FAILURE,
} from './ideaConsts.js';

const initialState = {
    fetching: {
        fetchIdeas: false,
    },
    ideas: [],
    error: null,
};

const ideaReducer = (state = initialState, action) => {
    const { type, payload } = action;
    switch (type) {
        case GET_IDEAS:
            return {
                ...state,
                fetching: { ...state.fetching, fetchIdeas: true },
            };
        case GET_IDEAS_SUCCESS:
            return {
                ...state,
                fetching: { ...state.fetching, fetchIdeas: false },
                ideas: payload,
            };
        case GET_IDEAS_FAILURE:
            return {
                ...state,
                fetching: { ...state.fetching, fetchIdeas: false },
                error: payload,
            };
        default:
            return state;
    }
};

export default ideaReducer;
