import { createAction } from 'redux-actions';

import {
    GET_IDEAS,
    GET_IDEAS_SUCCESS,
    GET_IDEAS_FAILURE,
} from './ideaConsts.js';

import {
    addErrorNotification,
    addFetchErrorNotification,
} from '../actions';

import { getURL } from '../env.js';

const ideasURL = getURL('ideasUrl');

export const getIdeas = createAction(GET_IDEAS);
export const getIdeasSuccess = createAction(GET_IDEAS_SUCCESS);
export const getIdeasFailure = createAction(GET_IDEAS_FAILURE);

// complex actions
export function fetchIdeas() {
    //const url = ideasURL;
    const url = '/cast/board/266/idea/?format=json';
    return (dispatch) => {
        dispatch(getIdeas());
        return fetch(url, {
            method: 'GET',
            credentials: 'include',
            redirect: 'follow',
            headers: { 'Content-Type': 'application/json' },
        })
        .then((res) => {
            console.log(res);
            if (res.ok) {
                res.json().then((json) => {
                    dispatch(getIdeasSuccess(json));
                });
            } else {
                dispatch(getIdeasFailure());
                dispatch(addFetchErrorNotification(res));
            }
        })
        .catch((err) => {
            dispatch(getIdeasFailure());
            dispatch(addErrorNotification(err));
        });
    };
}
