import React, { Component, PropTypes as Types } from 'react';

import { ideaFetchingStatePropType, ideaObjectPropType } from '../../customPropTypes';

class IdeaList extends Component {

    componentWillMount() {
        this.props.fetchIdeas();
    }

    render() {
        const {
            ideas,
        } = this.props;
        //console.log(ideas);

        return (
            <ul>
                {ideas.map((idea, i) => <li key={i}>{idea.name}</li>)}
            </ul>
        );
    }
}

IdeaList.propTypes = {
    ideas: Types.arrayOf(ideaObjectPropType).isRequired,
    fetching: ideaFetchingStatePropType.isRequired,
    fetchIdeas: Types.func.isRequired,
    addFetchErrorNotification: Types.func,
    addErrorNotification: Types.func,
};

export default IdeaList;
