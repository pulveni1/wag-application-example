import { connect } from 'react-redux';
import {
    fetchIdeas,
} from '../ideaActions';

import {
    addFetchErrorNotification,
    addErrorNotification,
} from '../../actions';

import IdeaList from '../components/IdeaList';

const mapStateToProps = function (state) {
    return {
        ideas: state.idea.ideas,
        fetching: state.idea.fetching,
    };
};

const mapDispatchToProps = ({
    fetchIdeas,
    addFetchErrorNotification,
    addErrorNotification,
});

const IdeaViewContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(IdeaList);

export default IdeaViewContainer;

