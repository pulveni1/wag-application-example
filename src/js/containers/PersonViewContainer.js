import { connect } from 'react-redux';

import * as PersonFilterTypes from '../enums/PersonFilterTypes';

import {
    setActivePerson,
    getPeople,
    createPerson,
    updatePerson,
    deletePerson,
} from '../redux/person/actions';

import {
    dispatchFetchErrorNotification,
    dispatchErrorNotification,
} from '../redux/notifications/actions';

import PersonView from '../pages/person/PersonView';

const getVisiblePersons = (filterArray, filter) => {
    let returnArray = filterArray;
    switch (filter) {
        case PersonFilterTypes.SHOW_ALL:
            returnArray = filterArray;
            break;
        case PersonFilterTypes.SHOW_INTERNAL:
            returnArray = filterArray.filter(p => p.functionalTitle !== 'EXTERNAL');
            break;
        case PersonFilterTypes.SHOW_EXTERNAL:
            returnArray = filterArray.filter(p => p.functionalTitle === 'EXTERNAL');
            break;
        default:
            throw new Error(`Unknown person filter: ${filter}`);
    }

    return returnArray.map(obj => obj.id);
};

const mapStateToProps = (state) => {
    const {
        network,
        peopleById,
        peopleIds,
        activePersonId,
        visibilityFilter,
    } = state.person;

    const filterArray = peopleIds.map((id) => {
        const person = peopleById[id];
        return {
            id: person.id,
            functionalTitle: person.functionalTitle,
        };
    });

    return {
        network,
        peopleById,
        activePersonId,
        visiblePeopleIds: getVisiblePersons(filterArray, visibilityFilter),
    };
};

const mapDispatchToProps = ({
    setActivePerson,
    getPeople,
    createPerson,
    updatePerson,
    deletePerson,
    dispatchFetchErrorNotification,
    dispatchErrorNotification,
});

const PersonViewContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(PersonView);

export default PersonViewContainer;

