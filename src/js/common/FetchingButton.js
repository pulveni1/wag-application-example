import React, { PropTypes as Types } from 'react';
import { Button } from 'react-bootstrap';
import { NxBarsLoader } from 'nxc-core';

const FetchingButton = (props) => {
    const { fetching, value, onClick, disabled } = props;
    return (
        <Button
          onClick={() => {
              if (!fetching) {
                  onClick();
              }
          }}
          disabled={disabled || fetching}
        >{fetching ? <NxBarsLoader /> : value}</Button>
    );
};

FetchingButton.defaultProps = {
    disabled: false,
};

FetchingButton.propTypes = {
    fetching: Types.bool.isRequired,
    value: Types.string.isRequired,
    onClick: Types.func.isRequired,
    disabled: Types.bool,
};

export default FetchingButton;
