import React from 'react';
import { Route, IndexRoute } from 'react-router';

import * as Paths from '../enums/Paths';

import MainViewContainer from '../containers/MainViewContainer';
import NotificationViewContainer from '../containers/NotificationViewContainer';
import PersonViewContainer from '../containers/PersonViewContainer';

import AboutView from '../pages/about/AboutView';
import NotFound from '../common/NotFound';

export default (
    <Route path={Paths.INDEX} component={MainViewContainer}>
        <IndexRoute component={AboutView} />
        <Route path={Paths.NOTIFICATIONS} component={NotificationViewContainer} />
        <Route path={Paths.PERSON} component={PersonViewContainer} />
        <Route path={Paths.ALL} component={NotFound} />
    </Route>
);
