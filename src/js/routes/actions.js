import { push } from 'react-router-redux';

const routeToPath = (route) => {  // eslint-disable-line arrow-body-style
    return (dispatch) => {
        dispatch(push(route));
    };
};

export {
    routeToPath,
};

export default routeToPath;
