import React, { Component } from 'react';
import { PropTypes } from 'prop-types';

import { Button } from 'react-bootstrap';

class AboutView extends Component {
    static propTypes = {
        notifications: PropTypes.arrayOf(PropTypes.object).isRequired,
        dispatchErrorNotification: PropTypes.func.isRequired,
        dispatchFetchErrorNotification: PropTypes.func.isRequired,
        dispatchSuccessNotification: PropTypes.func.isRequired,
        dispatchInfoNotification: PropTypes.func.isRequired,
        dispatchWarningNotification: PropTypes.func.isRequired,
    };

    triggerNotifications() {
        const {
            dispatchErrorNotification,
            dispatchFetchErrorNotification,
            dispatchSuccessNotification,
            dispatchInfoNotification,
            dispatchWarningNotification,
        } = this.props;

        dispatchErrorNotification({ message: 'Error message' });
        dispatchFetchErrorNotification({
            message: 'Fetch Error message',
            status: 400,
            statusText: 'Not found',
        });

        dispatchSuccessNotification({ title: 'Success', message: 'success message' });
        dispatchInfoNotification({ title: 'Info', message: 'info message' });
        dispatchWarningNotification({ title: 'Warning', message: 'warning message' });
    }

    render() {
        const { notifications } = this.props;

        const listItems = notifications.map((notification, i) => (
            <li key={i}>{JSON.stringify(notification)}</li>
        ));

        return (
            <div>
                <h1>Notification</h1>
                <p>Trigger notifications with click on the button below</p>

                <Button
                  bsStyle="primary"
                  onClick={::this.triggerNotifications}
                >Trigger Notifications</Button>

                <br /><br />
                <strong>All notifications</strong>
                <ul>{listItems}</ul>
            </div>
        );
    }
}

export default AboutView;
