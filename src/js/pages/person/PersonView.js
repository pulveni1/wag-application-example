import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Grid, Row, Col } from 'react-bootstrap';

import PersonList from './PersonList';
import PersonDetailView from './PersonDetailView';
import PersonFilter from './PersonFilter';
import PeopleSuggest from './PeopleSuggest';

class PersonView extends Component {
    static propTypes = {
        network: PropTypes.shape({}).isRequired,
        peopleById: PropTypes.shape({}).isRequired,
        visiblePeopleIds: PropTypes.arrayOf(PropTypes.number).isRequired,
        activePersonId: PropTypes.number,
        setActivePerson: PropTypes.func.isRequired,
        getPeople: PropTypes.func.isRequired,
        createPerson: PropTypes.func.isRequired,
        updatePerson: PropTypes.func.isRequired,
        deletePerson: PropTypes.func.isRequired,
        dispatchFetchErrorNotification: PropTypes.func.isRequired,
        dispatchErrorNotification: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);

        this.onPeopleSuggestSubmit = this.onPeopleSuggestSubmit.bind(this);
        this.onListItemClick = this.onListItemClick.bind(this);
        this.onListemItemIconClick = this.onListemItemIconClick.bind(this);
    }

    componentWillMount() {
        this.props.getPeople();
    }

    onPeopleSuggestSubmit(person) {
        this.props.setActivePerson(person);
    }

    onListItemClick(person) {
        this.props.setActivePerson(person);
    }

    onListemItemIconClick(person, e) {
        e.stopPropagation();
        this.props.deletePerson(person);
    }

    render() {
        const {
            peopleById,
            visiblePeopleIds,
            activePersonId,
            network,
            createPerson,
            updatePerson,
            dispatchFetchErrorNotification,
            dispatchErrorNotification,
        } = this.props;

        const activePerson = peopleById[activePersonId] || {};

        return (
            <Grid fluid>
                <Row>
                    <Col xs={12}>
                        <h1>Person List</h1>
                    </Col>
                </Row>
                <Row>
                    <Col sm={4} md={3}>
                        <h3>Search Person</h3>
                        <PeopleSuggest
                          onSubmit={this.onPeopleSuggestSubmit}
                          onError={dispatchErrorNotification}
                          onFetchError={dispatchFetchErrorNotification}
                        />
                        <h3>Contact list</h3>
                        <PersonFilter />
                        <PersonList
                          peopleById={peopleById}
                          peopleIds={visiblePeopleIds}
                          network={network}
                          onListItemClick={this.onListItemClick}
                          onListemItemIconClick={this.onListemItemIconClick}
                        />
                    </Col>
                    <Col sm={8} md={9}>
                        <h3>Show Person Details</h3>
                        <PersonDetailView
                          person={activePerson}
                          onAddPerson={createPerson}
                          onUpdatePerson={updatePerson}
                          network={network}
                        />
                    </Col>
                </Row>
            </Grid>
        );
    }
}

export default PersonView;
