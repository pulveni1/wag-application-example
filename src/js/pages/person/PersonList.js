import React from 'react';
import { PropTypes } from 'prop-types';
import { NxBarsLoader } from 'nxc-core';

import * as FetchStates from '../../enums/FetchStates';
import PersonListItem from './PersonListItem';

const isFetching = (network) => {
    const { getPeople } = network;

    return (
        (getPeople && getPeople.state === FetchStates.LOADING)
    );
};

const PersonList = ({ network, peopleById, peopleIds, onListItemClick, onListemItemIconClick }) => {
    if (isFetching(network)) {
        return (<div className="bars-loader-container">
            <div className="bars">
                <NxBarsLoader />
            </div>
        </div>);
    }

    return (
        <ul className="person-list">
        { (isFetching(network)) ?
            (
                <div className="bars-loader-container">
                    <div className="bars">
                        <NxBarsLoader />
                    </div>
                </div>
            ) : (
                peopleIds.map(personId => {
                    const person = peopleById[personId];
                    return (
                        <PersonListItem
                          person={person}
                          key={personId}
                          onClick={() => onListItemClick(person)}
                          onIconClick={e => onListemItemIconClick(person, e)}
                        />
                    );
                })
            )
        }
        </ul>
    );
};

PersonList.propTypes = {
    network: PropTypes.shape({}).isRequired,
    peopleById: PropTypes.shape({}).isRequired,
    peopleIds: PropTypes.arrayOf(PropTypes.number).isRequired,
    onListItemClick: PropTypes.func.isRequired,
    onListemItemIconClick: PropTypes.func.isRequired,
};

export default PersonList;
