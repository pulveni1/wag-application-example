import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Button, FormControl } from 'react-bootstrap';

import * as FetchStates from '../../enums/FetchStates';

import { personObjectPropType } from '../../common/customPropTypes';
import FetchingButton from '../../common/FetchingButton';

class PersonDetailView extends Component {
    static propTypes = {
        person: personObjectPropType,
        onAddPerson: PropTypes.func.isRequired,
        onUpdatePerson: PropTypes.func.isRequired,
        network: PropTypes.shape({}),
    };

    constructor(props) {
        super(props);

        this.onAddPersonClick = this.onAddPersonClick.bind(this);
        this.onUpdatePersonClick = this.onUpdatePersonClick.bind(this);
        this.onCancelUpdatePersonClick = this.onCancelUpdatePersonClick.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillMount() {
        this.resetState();
    }

    componentWillReceiveProps(newProps) {
        this.resetState(newProps);
    }

    onAddPersonClick() {
        const { person, onAddPerson } = this.props;
        onAddPerson(person);
    }

    onUpdatePersonClick() {
        const { comments } = this.state;
        const { person, onUpdatePerson } = this.props;
        const newPerson = Object.assign({}, person, { comments });
        onUpdatePerson(newPerson);
    }

    onCancelUpdatePersonClick() {
        this.resetState();
    }

    resetState(newProps) {
        const props = newProps || this.props;
        this.setState({
            comments: props.person.comments,
            editing: false,
        });
    }

    /**
     *   using high-order function to handle onChange event for multiple input events.
     *   key specifies the state key to set input value
     **/
    handleChange(key) {
        return (e) => {
            const newState = {
                editing: true,
            };
            newState[key] = e.target.value;
            this.setState(newState);
        };
    }

    renderButtons() {
        const { editing } = this.state;
        const { person, network } = this.props;
        const className = 'person-actions';
        if (!person.id) {
            return (
                <div className={className}>
                    <FetchingButton
                      onClick={this.onAddPersonClick}
                      value="Add to list"
                      fetching={network.createPerson === FetchStates.LOADING}
                    />
                </div>
            );
        }

        return (
            <div className={className}>
                <FetchingButton
                  onClick={this.onUpdatePersonClick}
                  value="Update"
                  fetching={network.updatePerson === FetchStates.LOADING}
                  disabled={!editing}
                />
                <Button
                  onClick={this.onCancelUpdatePersonClick}
                  disabled={!editing}
                >Cancel</Button>
            </div>
        );
    }

    renderCommentsSection(comments) {
        return (
            <div>
                <h4>Comments</h4>
                <FormControl
                  componentClass="textarea"
                  placeholder="Please add comments..."
                  value={comments}
                  onChange={this.handleChange('comments')}
                />
            </div>
        );
    }

    render() {
        const { comments } = this.state;
        const { person } = this.props;
        const keys = Object.keys(person);

        if (keys.length === 0) {
            return <pre>Nothing to see here... </pre>;
        }

        return (
            <div>
                {::this.renderButtons()}
                <pre>
                    <ul className="person-details-list">
                        { keys.filter(k => k !== 'comments').map((key, i) => (
                            <li key={i}>
                                <span>{key}</span>{person[key]}
                            </li>
                        ))
                        }
                    </ul>
                </pre>
                { person.id && this.renderCommentsSection(comments) }
            </div>
        );
    }
}

export default PersonDetailView;
