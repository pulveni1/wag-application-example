import React from 'react';

import * as PersonFilterTypes from '../../enums/PersonFilterTypes';
import PersonFilterContainer from '../../containers/PersonFilterContainer';

const PersonFilter = () => (
  <p className="filter">
    Show:
    {' '}
    <PersonFilterContainer filter={PersonFilterTypes.SHOW_ALL}>
      All
    </PersonFilterContainer>
    {', '}
    <PersonFilterContainer filter={PersonFilterTypes.SHOW_INTERNAL}>
      Internal
    </PersonFilterContainer>
    {', '}
    <PersonFilterContainer filter={PersonFilterTypes.SHOW_EXTERNAL}>
      External
    </PersonFilterContainer>
  </p>
);

export default PersonFilter;
