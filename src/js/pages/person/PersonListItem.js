import React from 'react';
import { PropTypes } from 'prop-types';
import { personObjectPropType } from '../../common/customPropTypes';

const PersonListItem = ({ person, onClick, onIconClick }) => (
    <li>
        <a
          key={person.id}
          onClick={onClick}
          tabIndex={person.id}
        >
            <span>{person.firstname} {person.lastname}</span>
            <i
              className="fa fa-trash-o"
              aria-hidden="true"
              onClick={onIconClick}
            />
        </a>
    </li>
);

PersonListItem.propTypes = {
    person: personObjectPropType.isRequired,
    onClick: PropTypes.func.isRequired,
    onIconClick: PropTypes.func.isRequired,
};

export default PersonListItem;
