import React, { Component } from 'react';
import NxHeader from 'nxc-core/NxHeader';
import PropTypes from 'prop-types';
import Notifications from 'react-notification-system-redux';

import MainMenu from './MainMenu';
import Footer from './Footer';

class MainView extends Component {

    static propTypes = {
        children: PropTypes.node,
        notifications: PropTypes.arrayOf(PropTypes.object),
        routeToPath: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {
            person: '',
        };
    }

    handleAuth(person) {
        this.setState({ person });
    }

    render() {
        const getHelpOptions = {
            subHeading: 'How can the WAE team help you?',
            subHeadingText: 'Please fill out the form to contact the WAE team about issues or feature requests.',
            otherResources: (
                <ul style={{ listStyle: 'none', paddingLeft: '0px' }}>
                    <li>
                        <a
                          target="_blank"
                          rel="noopener noreferrer"
                          href="http://go/wag-docs"
                        >WAG Documentation</a>
                    </li>
                </ul>
            ),
            channelOptions: {
                issue: {
                    type: 'JIRA',
                    value: {
                        issueTypeName: 'Bug',
                        defaultAssignee: 'westeja2',
                        projectCode: 'WAG',
                    },
                },
                request: {
                    type: 'Email',
                    // this will fail, so change it or remove
                    value: 'ENTER_VALID_EMAIL@novartis.com',
                },
                feedback: {
                    type: 'JIRA',
                    value: {
                        issueTypeName: 'Feature',
                        defaultAssignee: 'westeja2',
                        projectCode: 'WAG',
                    },
                },
            },
        };

        const { notifications, routeToPath } = this.props;

        return (
            <div className="application-wrapper">
                <div className="content">
                    <div style={{ paddingBottom: '1em' }}>
                        <NxHeader
                          appName={'WAE'}
                          description={'wag-application-example'}
                          deployState={'alpha'}
                          titleHref={'#/'}
                          onAuth={::this.handleAuth}
                          deployHref={''}
                          getHelpOptions={getHelpOptions}
                        />
                        <MainMenu
                          routeToPath={routeToPath}
                        />
                    </div>
                    <Notifications
                      notifications={notifications}
                    />
                    { this.props.children }
                </div>
                <Footer />
            </div>
        );
    }
}

export default MainView;
