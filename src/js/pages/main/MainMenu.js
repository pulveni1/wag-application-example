import React, { Component } from 'react';
import DocumentTitle from 'react-document-title';
import PropTypes from 'prop-types';
import { MenuItem, Navbar, Nav, NavItem, NavDropdown } from 'react-bootstrap';

import * as Paths from '../../enums/Paths';

const NibrNavbar = Navbar;
NibrNavbar.propTypes = {
    bsStyle: React.PropTypes.oneOf(['default', 'nibr']),
};

class MainMenu extends Component {
    static propTypes = {
        routeToPath: PropTypes.func,
    };

    constructor(props) {
        super(props);

        this.customRouting = this.customRouting.bind(this);
    }

    customRouting() {
        console.log('customRouting');
        this.props.routeToPath(Paths.INDEX);
    }

    render() {
        return (
            <NibrNavbar fluid bsStyle="nibr">
                <DocumentTitle title="WAE" />
                <Nav>
                    <NavItem eventKey="1" href={`#${Paths.INDEX}`}>Home</NavItem>
                    <NavItem eventKey="2" href={`#${Paths.PERSON}`}>Person List</NavItem>
                    <NavItem eventKey="3" href={`#${Paths.NOTIFICATIONS}`}>Notifications</NavItem>
                    <NavDropdown id="Routing" title="Routing">
                        <MenuItem
                          eventKey="4"
                          onClick={this.customRouting}
                        >Custom Routing</MenuItem>
                        <MenuItem eventKey="5" href={`#${Paths.FAIL}`}>Routing Fail Example</MenuItem>
                    </NavDropdown>
                </Nav>
            </NibrNavbar>
        );
    }
}

export default MainMenu;
