export const INDEX = '/';
export const ABOUT = '/about';
export const PERSON = '/person';
export const NOTIFICATIONS = '/notifications';
export const FAIL = '/fail';

export const ALL = '*';
