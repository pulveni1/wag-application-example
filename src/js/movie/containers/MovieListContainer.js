import {connect} from 'react-redux';

import {
    onMovieSelected,
    onSearchTextChanged,
} from '../movieActions';

import MovieList from '../components/MovieList';


const mapStateToProps = function (state) {
    return {
        movies: state.movie.movies,
        selectedMovieIndex: state.movie.selectedMovieIndex,
        searchText: state.movie.searchText,
        filteredMovies: state.movie.movies.filter((movie) => {
            const normalizedMovieName = movie.name.toLowerCase();
            const normalizedSearchText = state.movie.searchText.toLowerCase();
            return normalizedMovieName.indexOf(normalizedSearchText) >= 0;
        }),
    };
};

const mapDispatchToProps = ({
    onMovieSelected,
    onSearchTextChanged,
});

const MovieListContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(MovieList);

export default MovieListContainer;
