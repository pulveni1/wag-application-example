import {
    MOVIE_SELECTED,
    SEARCH_TEXT_CHANGED,
} from './movieConsts';

const initialState = {
    selectedMovieIndex: 0,
    searchText: '',
    movies: [
        { id: 0, name: 'Kill Bill I' },
        { id: 1, name: 'James Bond: Moonraker' },
        { id: 2, name: 'Rambo I' },
    ],
};

const movieReducer = (state = initialState, action) => {
    const { type, payload } = action;
    switch (type) {
        case MOVIE_SELECTED:
            return {
                ...state,
                selectedMovieIndex: payload,
            };
        case SEARCH_TEXT_CHANGED:
            return {
                ...state,
                searchText: payload,
            };
        default:
            return state;
    }
};

export default movieReducer;

