import { createAction } from 'redux-actions';

import {
    MOVIE_SELECTED,
    SEARCH_TEXT_CHANGED,
} from './movieConsts';

export const movieSelected = createAction(MOVIE_SELECTED);
export const onSearchTextChanged = createAction(SEARCH_TEXT_CHANGED);

export function onMovieSelected(movieId) {
    return (dispatch) => {
        dispatch(movieSelected(movieId));
    };
}
