import React, { Component, PropTypes as Types } from 'react';

import MovieSearch from './MovieSearch';

class MovieList extends Component {
    static propTypes = {
        movies: Types.arrayOf(Types.object).isRequired,
        filteredMovies: Types.arrayOf(Types.object).isRequired,
        selectedMovieIndex: Types.number.isRequired,
        searchText: Types.string,
        onMovieSelected: Types.func.isRequired,
        onSearchTextChanged: Types.func.isRequired,
    };

    getLabelStyle(movieId) {
        if (this.props.searchText.length === 0 && this.props.selectedMovieIndex === movieId) {
            return {
                fontWeight: 'bold',
                cursor: 'default',
            };
        }
        return {
            fontWeight: 'normal',
            cursor: 'pointer',
        };
    }

    render() {
        const {
            filteredMovies,
            searchText,
            onMovieSelected,
            onSearchTextChanged,
        } = this.props;

        return (
            <div className="movie-list-view">
                <MovieSearch searchText={searchText} onSearchTextChanged={onSearchTextChanged} />
                <ul className="movie-list">
                    { filteredMovies.map(movie => (
                        <li key={movie.id}>
                            <div
                              onClick={() => onMovieSelected(movie.id)}
                              style={this.getLabelStyle(movie.id)}
                            >
                                <div>
                                    Movie Name:
                                </div>
                                <div>
                                    { movie.name }
                                </div>
                            </div>
                        </li>
                    ))}
                </ul>
            </div>
        );
    }
}

export default MovieList;
