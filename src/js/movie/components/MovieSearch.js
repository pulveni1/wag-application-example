import React, {Component, PropTypes as Types} from 'react';

class MovieSearch extends Component {
    static propTypes = {
        searchText: Types.string,
        onSearchTextChanged: Types.func.isRequired,
    };

    render() {
        const {
            searchText,
            onSearchTextChanged,
        } = this.props;

        return (
            <input type="text" value={searchText} onChange={(event) => { onSearchTextChanged(event.target.value); }} className="movie-search-box" />
        );
    };
}

export default MovieSearch;
