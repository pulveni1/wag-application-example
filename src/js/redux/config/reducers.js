import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import ideaReducer from './idea/ideaReducer';
import movieReducer from './movie/movieReducer';
import notificationsReducer from '../notifications/reducer';
import personReducer from '../person/reducer';

export default combineReducers({
    routing: routerReducer,
    notifications: notificationsReducer,
    person: personReducer,
    idea: ideaReducer,
    movie: movieReducer,
});
