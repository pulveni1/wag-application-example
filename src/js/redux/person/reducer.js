import * as Consts from './consts';
import * as FetchStates from '../../enums/FetchStates';

import * as PersonFilterTypes from '../../enums/PersonFilterTypes';

const peopleById = (state = {}, action) => {
    let newState = { ...state };
    const { type, payload } = action;
    switch (type) {
        case Consts.GET_PEOPLE_REQUEST_SUCCESS:
            return payload.peopleById;
        case Consts.UPDATE_PERSON_REQUEST_SUCCESS:
        case Consts.CREATE_PERSON_REQUEST_SUCCESS:
            newState = {
                ...state,
            };
            newState[payload.id] = payload;
            return newState;
        case Consts.DELETE_PERSON_REQUEST_SUCCESS:
            newState = {
                ...state,
            };
            delete newState[payload.id];
            return newState;
        case Consts.SET_ACTIVE_PERSON:
            newState = {
                ...state,
            };
            newState[0] = payload;
            return newState;
        default:
            return state;
    }
};

const peopleIds = (state = [], action) => {
    const { type, payload } = action;
    let newArray;
    switch (type) {
        case Consts.GET_PEOPLE_REQUEST_SUCCESS:
            return payload.peopleIds;
        case Consts.CREATE_PERSON_REQUEST_SUCCESS:
            return [...state, payload.id];
        case Consts.DELETE_PERSON_REQUEST_SUCCESS:
            newArray = [...state];
            newArray.splice(newArray.indexOf(payload.id), 1);
            return newArray;
        default:
            return state;
    }
};

const visibilityFilter = (state = PersonFilterTypes.SHOW_ALL, action) => {
    const { type, payload } = action;
    switch (type) {
        case Consts.SET_VISIBILITY_FILTER:
            return payload;
        default:
            return state;
    }
};

const activePersonId = (state, action) => {
    const { type, payload } = action;
    switch (type) {
        case Consts.SET_ACTIVE_PERSON:
            return payload.id || 0; // if not id use 0;
        case Consts.CREATE_PERSON_REQUEST_SUCCESS:
            return payload.id;
        default:
            return state;
    }
};

const networkHandling = (state = {}, action) => {
    const { type, payload, key } = action;
    const newState = { ...state };
    switch (type) {
        case Consts.GET_PEOPLE_REQUEST:
        case Consts.CREATE_PERSON_REQUEST:
        case Consts.UPDATE_PERSON_REQUEST:
            newState[key] = {
                state: FetchStates.LOADING,
            };
            break;
        case Consts.GET_PEOPLE_REQUEST_SUCCESS:
        case Consts.CREATE_PERSON_REQUEST_SUCCESS:
        case Consts.UPDATE_PERSON_REQUEST_SUCCESS:
            newState[key] = {
                state: FetchStates.SUCCESS,
            };
            break;
        case Consts.GET_PEOPLE_REQUEST_FAILURE:
        case Consts.CREATE_PERSON_REQUEST_FAILURE:
        case Consts.UPDATE_PERSON_REQUEST_FAILURE:
            newState[key] = {
                state: FetchStates.ERROR,
                error: payload,
            };
            break;
        default:
    }
    return newState;
};

const personReducer = (state = {}, action) => ({
    network: networkHandling(state.network, action),
    peopleById: peopleById(state.peopleById, action),
    peopleIds: peopleIds(state.peopleIds, action),
    visibilityFilter: visibilityFilter(state.visibilityFilter, action),
    activePersonId: activePersonId(state.activePersonId, action),
});

export default personReducer;
