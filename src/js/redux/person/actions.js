import { createAsyncAction } from '../config/reduxUtils';
import { getURL } from '../../common/fetchUtils';
import * as Consts from './consts';

import {
    dispatchErrorNotification,
    dispatchFetchErrorNotification,
} from '../notifications/actions';

const AsyncKeys = {
    getPeople: 'getPeople',
    createPerson: 'createPerson',
    updatePerson: 'updatePerson',
    deletePerson: 'deletePerson',
};

const personUrl = getURL('personUrl');

const setActivePerson = (activePerson) => ({
    type: Consts.SET_ACTIVE_PERSON,
    payload: activePerson,
});
const setVisibilityFilter = (filter) => ({
    type: Consts.SET_VISIBILITY_FILTER,
    payload: filter,
});

const getPeopleRequest = createAsyncAction(AsyncKeys.getPeople, Consts.GET_PEOPLE_REQUEST);
const getPeopleRequestFailure = createAsyncAction(AsyncKeys.getPeople, Consts.GET_PEOPLE_REQUEST_FAILURE); // eslint-disable-line max-len
const getPeopleRequestSuccess = (data) => {
    const peopleById = {};
    const peopleIds = [];

    // proceed persons array
    data.forEach((people) => {
        peopleById[people.id] = people;
        peopleIds.push(people.id);
    });

    const payload = {
        peopleById,
        peopleIds,
    };

    return {
        type: Consts.GET_PEOPLE_REQUEST_SUCCESS,
        key: AsyncKeys.getPeople,
        payload,
    };
};

const createPersonRequest = createAsyncAction(AsyncKeys.createPerson, Consts.CREATE_PERSON_REQUEST);
const createPersonRequestFailure = createAsyncAction(AsyncKeys.createPerson, Consts.CREATE_PERSON_REQUEST_FAILURE); // eslint-disable-line max-len
const createPersonRequestSuccess = (person) => ({
    type: Consts.CREATE_PERSON_REQUEST_SUCCESS,
    key: AsyncKeys.createPerson,
    payload: person,
});

const updatePersonRequest = createAsyncAction(AsyncKeys.updatePerson, Consts.UPDATE_PERSON_REQUEST);
const updatePersonRequestFailure = createAsyncAction(AsyncKeys.updatePerson, Consts.UPDATE_PERSON_REQUEST_FAILURE); // eslint-disable-line max-len
const updatePersonRequestSuccess = (person) => ({
    type: Consts.UPDATE_PERSON_REQUEST_SUCCESS,
    key: AsyncKeys.updatePerson,
    payload: person,
});

const deletePersonRequest = createAsyncAction(AsyncKeys.deletePerson, Consts.DELETE_PERSON_REQUEST);
const deletePersonRequestFailure = createAsyncAction(AsyncKeys.deletePerson, Consts.DELETE_PERSON_REQUEST_FAILURE); // eslint-disable-line max-len
const deletePersonRequestSuccess = (person) => ({
    type: Consts.DELETE_PERSON_REQUEST_SUCCESS,
    key: AsyncKeys.deletePerson,
    payload: person,
});

// complex actions
const getPeople = () => {
    const url = personUrl;
    return (dispatch) => {
        dispatch(getPeopleRequest());
        return fetch(url, {
            method: 'GET',
            mode: 'cors',
            credentials: 'include',
        })
            .then((res) => {
                if (res.ok) {
                    res.json().then((json) => {
                        dispatch(getPeopleRequestSuccess(json));
                    });
                } else {
                    dispatch(getPeopleRequestFailure());
                    dispatch(dispatchFetchErrorNotification(res));
                }
            })
            .catch((err) => {
                dispatch(getPeopleRequestFailure());
                dispatch(dispatchErrorNotification(err));
            });
    };
};

const createPerson = (person) => {
    const url = personUrl;
    return (dispatch) => {
        dispatch(createPersonRequest());
        return fetch(url, {
            method: 'POST',
            mode: 'cors',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(person),
        })
            .then((res) => {
                if (res.ok) {
                    res.json().then((json) => {
                        dispatch(createPersonRequestSuccess(json));
                    });
                } else {
                    dispatch(createPersonRequestFailure());
                    dispatch(dispatchFetchErrorNotification(res));
                }
            })
            .catch((err) => {
                dispatch(createPersonRequestFailure(err));
                dispatch(dispatchErrorNotification(err));
            });
    };
};

const updatePerson = (person) => {
    const url = `${personUrl}/${person.id}`;
    return (dispatch) => {
        dispatch(updatePersonRequest());
        return fetch(url, {
            method: 'PUT',
            mode: 'cors',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(person),
        })
            .then((res) => {
                if (res.ok) {
                    res.json().then((json) => {
                        dispatch(updatePersonRequestSuccess(json));
                    });
                } else {
                    dispatch(updatePersonRequestFailure());
                    dispatch(dispatchFetchErrorNotification(res));
                }
            })
            .catch((err) => {
                dispatch(updatePersonRequestFailure(err));
                dispatch(dispatchErrorNotification(err));
            });
    };
};

const deletePerson = (person) => {
    const url = `${personUrl}/${person.id}`;
    return (dispatch) => {
        dispatch(deletePersonRequest());
        return fetch(url, {
            method: 'DELETE',
            mode: 'cors',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(person),
        })
            .then((res) => {
                if (res.ok) {
                    dispatch(deletePersonRequestSuccess(person));
                } else {
                    dispatch(deletePersonRequestFailure());
                    dispatch(dispatchFetchErrorNotification(person));
                }
            })
            .catch((err) => {
                dispatch(deletePersonRequestFailure(err));
                dispatch(dispatchErrorNotification(err));
            });
    };
};

export {
    setActivePerson,
    setVisibilityFilter,
    getPeople,
    createPerson,
    updatePerson,
    deletePerson,
};
